# Thesis-Discussion

The slideshow used in my Bachelor Degree's final exam. It has been realized using only free-software, such as [LibreOffice Impress](https://www.libreoffice.org/) and [Evince](https://wiki.gnome.org/Apps/Evince).
